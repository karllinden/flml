/*
 * This file is part of flml.
 *
 * Copyright (C) 2018, 2021-2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef BENCH_H
#define BENCH_H

#if BENCH

#include <assert.h>
#include <limits.h>
#include <stdio.h>

#include "timespec.h"

struct bench {
    long min;
    long max;
    unsigned long n;
    double avg;
    struct timespec begin;
};

#define BENCH_DECLARE(name) struct bench name;

static inline void bench_init(struct bench *bench) {
    bench->min = LONG_MAX;
    bench->max = 0;
    bench->avg = 0;
    bench->n = 0;
    bench->begin.tv_sec = -1;
    bench->begin.tv_nsec = -1;
}

static inline void bench_begin(struct bench *bench) {
    assert(bench->begin.tv_sec == -1 && bench->begin.tv_nsec == -1);
    timespec_now(&bench->begin);
}

static inline void bench_end(struct bench *bench) {
    struct timespec end;
    struct timespec diff;
    double n;
    long nsec;

    assert(bench->begin.tv_sec != -1 || bench->begin.tv_nsec != -1);
    timespec_now(&end);
    timespec_diff(&diff, &end, &bench->begin);
    bench->begin.tv_sec = -1;
    bench->begin.tv_nsec = -1;

    nsec = diff.tv_sec * 1000000000 + diff.tv_nsec;

    if (nsec < bench->min) {
        bench->min = nsec;
    }

    if (nsec > bench->max) {
        bench->max = nsec;
    }

    n = (double)++bench->n;

    bench->avg *= (n - 1) / n;
    bench->avg += nsec / n;
}

static inline void bench_print(const struct bench *bench, const char *title) {
    printf("%s (n)  : %9lu\n", title, bench->n);
    printf("%s (min): %9ld nsecs\n", title, bench->min);
    printf("%s (max): %9ld nsecs\n", title, bench->max);
    printf("%s (avg): %9ld nsecs\n", title, (long)bench->avg);
}

static inline void bench_unprint(void) {
    for (int i = 0; i < 4; ++i) {
        fputs("\033[K", stdout);
        fputs("\033[1A", stdout);
    }
}

#else /* !BENCH */
#define BENCH_DECLARE(name) /* nothing */
#define bench_init(bench) /* nothing */
#define bench(bench, value) /* nothing */
#define bench_begin(bench) /* nothing */
#define bench_end(bench) /* nothing */
#define bench_print(bench, title) /* nothing */
#define bench_unprint() /* nothing */
#endif /* BENCH */

#endif /* !BENCH_H */
