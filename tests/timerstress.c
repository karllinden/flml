/*
 * This file is part of flml.
 *
 * Copyright (C) 2018-2019, 2021-2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <dima/system.h>

#include <flml/flml.h>

#include "src/flml.h"
#include "src/timer.h"
#include "src/timespec.h"

#include "bench.h"
#include "rng.h"
#include "rng_dima.h"
#include "swap_dima.h"

/**
 * Failure rate of the memory allocator.
 *
 * It is set fairly high to trigger re-registration errors.
 */
#define RNG_DIMA_FAILURE_RATE 50

/**
 * Run the stress test for at most four seconds.
 */
#define MAX_RUNTIME_MILLIS 4096

/**
 * The number of timers to start with initially.
 */
#define N_TIMERS_INIT 128

/**
 * The maximum number of timers to add when a callback is triggered.
 */
#define MAX_ADD_NEW 4

/**
 * The maximum relative time of a timer.
 */
#define MAX_TIMER_MILLIS 1024

/**
 * Whether or not to report statistics.
 */
#define REPORT 0

struct timerstress {
    struct flml *flml;

    /**
     * The upper bound of the runtime of this test.
     */
    struct timespec end;

    /**
     * The number of successful calls to try_register.
     */
    unsigned long n_try_register_success;

    /**
     * The number of failed calls to try_register.
     */
    unsigned long n_try_register_fail;

    /**
     * The number of calls to try_register that resulted in a timer to
     * far away in time, and thus were rejected.
     */
    unsigned long n_try_register_rejected;

    /**
     * The number of timers that have been triggered.
     */
    unsigned long n_triggered_timers;

    /**
     * The number of timers that have been triggered too early.
     */
    unsigned long n_early_triggers;

    /**
     * The average overrun of the expected trigger time  in
     * microseconds.
     */
    double avg_overrun_usec;

    /**
     * Benchmarks to do, if applicable.
     */
    BENCH_DECLARE(register_bench)
    BENCH_DECLARE(trigger_bench)
};

static void millis_to_timespec(unsigned long, struct timespec *);
static void rel_to_abs(struct timespec *, const struct timespec *);
static void get_end_time(struct timespec *);
static void randomize_timespec(struct timespec *);
static void callback(struct flml *, flml_timer_key);
static void try_register(struct timerstress *);

static void millis_to_timespec(unsigned long millis, struct timespec *res) {
    res->tv_sec = millis / 1000;
    res->tv_nsec = (millis % 1000) * 1000 * 1000;
}

static void rel_to_abs(struct timespec *abs, const struct timespec *rel) {
    flml_timespec_now(abs);
    int overflows = flml_timespec_add_non_negative(abs, rel);
    if (overflows) {
        flml_fatal("struct timespec would overflow");
    }
}

static void get_end_time(struct timespec *end) {
    struct timespec rel;
    millis_to_timespec(MAX_RUNTIME_MILLIS, &rel);
    rel_to_abs(end, &rel);
}

static void init(struct timerstress *ts, struct dima *dima) {
    struct flml_error error;

    ts->flml = flml_new_with_dima(dima, &error);
    assert(ts->flml);
    flml_set_ptr(ts->flml, ts);

    get_end_time(&ts->end);

    ts->n_try_register_success = 0;
    ts->n_try_register_fail = 0;
    ts->n_try_register_rejected = 0;
    ts->n_triggered_timers = 0;
    ts->n_early_triggers = 0;
    ts->avg_overrun_usec = 0;

    bench_init(&ts->register_bench);
    bench_init(&ts->trigger_bench);
}

static void deinit(struct timerstress *ts) {
    flml_destroy(ts->flml);
}

static void randomize_timespec(struct timespec *res) {
    unsigned millis = (unsigned)rng_min_max(1, MAX_TIMER_MILLIS);
    millis_to_timespec(millis, res);
}

static int should_register(const struct timerstress *ts,
                           const struct timespec *timer) {
    return flml_timespec_compare(timer, &ts->end) <= 0;
}

static void callback(struct flml *flml, flml_timer_key key) {
    struct timespec now;
    flml_timespec_now(&now);

    struct timespec *expected = flml_timer_get_ptr(flml, key);
    struct timerstress *ts = flml_get_ptr(flml);

    unsigned long n = ++ts->n_triggered_timers;

    if (flml_timespec_compare(expected, &now) < 0) {
        struct timespec diff;
        unsigned long diff_usec;

        flml_timespec_difference(&diff, &now, expected);
        diff_usec = diff.tv_sec * 1000 * 1000 + diff.tv_nsec / 1000;
        ts->avg_overrun_usec *= (double)(n - 1) / (double)n;
        ts->avg_overrun_usec += (double)diff_usec / (double)n;
    } else {
        ts->n_early_triggers++;
    }

    unsigned add_new = rng_min_max(0, MAX_ADD_NEW);
    for (unsigned i = 0; i < add_new; ++i) {
        try_register(ts);
    }

    free(expected);
}

static void try_register(struct timerstress *ts) {
    struct timespec *expected = malloc(sizeof(*expected));
    if (!expected) {
        perror("malloc");
        abort();
    }

    struct timespec timer;
    randomize_timespec(&timer);
    rel_to_abs(expected, &timer);

    if (!should_register(ts, expected)) {
        ts->n_try_register_rejected++;
        free(expected);
        return;
    }

    bench_begin(&ts->register_bench);
    flml_timer_key key = flml_timer_register(ts->flml, &timer, callback);
    bench_end(&ts->register_bench);
    if (flml_timer_key_is_error(key)) {
        ts->n_try_register_fail++;
        free(expected);
    } else {
        ts->n_try_register_success++;
        flml_timer_set_ptr(ts->flml, key, expected);
    }
}

static void print_report(const struct timerstress *ts UNUSED,
                         const struct timespec *next UNUSED) {
#if REPORT
    if (next->tv_sec >= 0 && next->tv_nsec >= 0) {
        unsigned long ns = next->tv_nsec;
        unsigned long us = ns / 1000;
        unsigned long ms = us / 1000;
        ns %= 1000;
        us %= 1000;

        printf("Next sleep: %lds %3ldms %3ldus %3ldns\n",
               next->tv_sec,
               ms,
               us,
               ns);
    } else {
        printf("Next sleep: none                \n");
    }

    printf("Triggered timers: %lu\n", ts->n_triggered_timers);
    printf("Timeouts triggered too early: %lu\n", ts->n_early_triggers);
    printf("Average overrun in usecs: %lu\n",
           (unsigned long)ts->avg_overrun_usec);
    printf("Successful calls to try_register: %lu\n",
           ts->n_try_register_success);
    printf("Failed calls to try_register: %lu\n", ts->n_try_register_fail);
    printf("Rejected calls to try_register: %lu\n",
           ts->n_try_register_rejected);

    bench_print(&ts->register_bench, "flml_timer_register");
    bench_print(&ts->trigger_bench, "flml_timers_trigger");
#endif
}

static void unprint_report(void) {
#if REPORT
    for (int i = 0; i < 7; ++i) {
        fputs("\033[K", stdout);
        fputs("\033[1A", stdout);
    }
    bench_unprint();
    bench_unprint();
#endif
}

static void run_test(void *ptr) {
    struct timerstress *ts = ptr;

    struct timespec next;
    next.tv_sec = 0;
    next.tv_nsec = 0;
    print_report(ts, &next);
    do {
        nanosleep(&next, NULL);

        next.tv_sec = -1;
        next.tv_nsec = -1;

        bench_begin(&ts->trigger_bench);
        flml_timers_trigger(ts->flml, &next);
        bench_end(&ts->trigger_bench);

        unprint_report();
        print_report(ts, &next);
    } while (next.tv_sec >= 0 && next.tv_nsec >= 0);
}

int main(void) {
    struct dima *system_instance = dima_system_instance();

    struct swap_dima swap_dima;
    swap_dima_init(&swap_dima, system_instance);

    struct timerstress ts;

    init(&ts, swap_dima_to_dima(&swap_dima));
    rng_seed();

    for (int i = 0; i < N_TIMERS_INIT; ++i) {
        try_register(&ts);
    }

    struct rng_dima dima;
    rng_dima_init(&dima, system_instance, RNG_DIMA_FAILURE_RATE);
    run_with_swapped_dima(&swap_dima, rng_dima_to_dima(&dima), run_test, &ts);

    deinit(&ts);

    return 0;
}
