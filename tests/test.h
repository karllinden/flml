/*
 * This file is part of flml.
 *
 * Copyright (C) 2018-2019, 2021-2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef TEST_H
#define TEST_H

#include <signal.h>

#include <check.h>

#define ADD_TEST(name) add_test(#name, name)
#define ADD_LOOP_TEST(name, start, end) add_loop_test(#name, name, start, end)
#define ADD_RAISE_SIGNAL_TEST(name, signal) \
    add_raise_signal_test(#name, name, signal)
#define ADD_ABORT_TEST(name) ADD_RAISE_SIGNAL_TEST(name, SIGABRT)

/* Provided by each of the test programs. */
void set_up(void);
void tear_down(void);
void add_tests(void);

/* Provided by test.c. */
void add_test(const char *name, TFun func);
void add_loop_test(const char *name, TFun func, int start, int end);
void add_raise_signal_test(const char *name, TFun func, int signal);

#endif /* !TEST_H */
