/*
 * This file is part of flml.
 *
 * Copyright (C) 2018, 2021-2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <limits.h>

#include "test.h"

#include "timespec.h"

void set_up(void) {
    /* empty */
}

void tear_down(void) {
    /* empty */
}

static void test_add(time_t tv_sec1,
                     long tv_nsec1,
                     time_t tv_sec2,
                     long tv_nsec2,
                     time_t tv_sec_expected,
                     long tv_nsec_expected) {
    struct timespec ts1 = {tv_sec1, tv_nsec1};
    struct timespec ts2 = {tv_sec2, tv_nsec2};

    int overflows = flml_timespec_add_non_negative(&ts1, &ts2);

    ck_assert_int_eq(overflows, 0);
    ck_assert_int_eq(ts1.tv_sec, tv_sec_expected);
    ck_assert_int_eq(ts1.tv_nsec, tv_nsec_expected);
}

static void test_add_overflow(time_t tv_sec1,
                              long tv_nsec1,
                              time_t tv_sec2,
                              long tv_nsec2) {
    struct timespec ts1 = {tv_sec1, tv_nsec1};
    struct timespec ts2 = {tv_sec2, tv_nsec2};

    int overflows = flml_timespec_add_non_negative(&ts1, &ts2);

    ck_assert_int_ne(overflows, 0);
}

static void test_to_millis(long tv_sec, long tv_nsec, int expected_millis) {
    struct timespec ts;
    int actual_millis;

    ts.tv_sec = tv_sec;
    ts.tv_nsec = tv_nsec;

    actual_millis = flml_timespec_to_millis(&ts);

    ck_assert_int_eq(actual_millis, expected_millis);
}

START_TEST(zero_timespec_is_non_negative) {
    struct timespec zero = {0, 0};
    ck_assert(flml_timespec_is_non_negative(&zero));
}
END_TEST

START_TEST(timespec_with_max_nsecs_is_non_negative) {
    struct timespec ts = {0, 999999999};
    ck_assert(flml_timespec_is_non_negative(&ts));
}
END_TEST

START_TEST(timespec_with_negative_sec_is_negative) {
    struct timespec ts = {-1, 0};
    ck_assert(!flml_timespec_is_non_negative(&ts));
}
END_TEST

START_TEST(timespec_with_negative_nsec_is_invalid) {
    struct timespec ts = {0, -1};
    ck_assert(!flml_timespec_is_non_negative(&ts));
}
END_TEST

START_TEST(timespec_with_too_large_nsec_is_invalid) {
    struct timespec ts = {0, 1000000000};
    ck_assert(!flml_timespec_is_non_negative(&ts));
}
END_TEST

START_TEST(add_zero) {
    test_add(0, 0, 0, 0, 0, 0);
}
END_TEST

START_TEST(add_secs) {
    test_add(3, 0, 2, 0, 5, 0);
}
END_TEST

START_TEST(add_nsecs) {
    test_add(0, 2348, 0, 7849, 0, 10197);
}
END_TEST

START_TEST(add_nsecs_with_carry) {
    test_add(0, 650000000, 0, 530000000, 1, 180000000);
}
END_TEST

START_TEST(add_close_to_max) {
    test_add(LONG_MAX - 2, 500000000, 2, 499999999, LONG_MAX, 999999999);
}
END_TEST

START_TEST(add_secs_overflow) {
    test_add_overflow(LONG_MAX - 7, 0, 8, 0);
}
END_TEST

START_TEST(add_secs_and_nsecs_overflow) {
    test_add_overflow(LONG_MAX - 7, 500000000, 7, 500000000);
}
END_TEST

START_TEST(to_millis_zero) {
    test_to_millis(0, 0, 0);
}
END_TEST

START_TEST(to_millis_secs) {
    test_to_millis(3, 0, 3000);
}
END_TEST

START_TEST(to_millis_nsecs) {
    test_to_millis(0, 2000000, 2);
}
END_TEST

START_TEST(to_millis_both) {
    test_to_millis(3, 456000000, 3456);
}
END_TEST

START_TEST(to_millis_nsecs_rounds_up) {
    test_to_millis(0, 1999999, 2);
}
END_TEST

START_TEST(to_millis_secs_overflow) {
    test_to_millis(INT_MAX / 1000 + 1, 0, INT_MAX);
}
END_TEST

START_TEST(to_millis_sum_overflows) {
    /*
     * In this test it is wanted that:
     *
     * secs*1000 + nsecs/1000000 > INT_MAX
     *
     * nsecs is set to 999000000 (999 milliseconds), so it must be that
     *
     * secs*1000 > INT_MAX - 999,
     *
     * whence
     *
     * secs = (INT_MAX - 999) / 1000 + 1
     *
     * works.
     */
    test_to_millis((INT_MAX - 999) / 1000 + 1, 999000000, INT_MAX);
}
END_TEST

void add_tests(void) {
    ADD_TEST(zero_timespec_is_non_negative);
    ADD_TEST(timespec_with_max_nsecs_is_non_negative);
    ADD_TEST(timespec_with_negative_sec_is_negative);
    ADD_TEST(timespec_with_negative_nsec_is_invalid);
    ADD_TEST(timespec_with_too_large_nsec_is_invalid);

    ADD_TEST(add_zero);
    ADD_TEST(add_secs);
    ADD_TEST(add_nsecs);
    ADD_TEST(add_nsecs_with_carry);
    ADD_TEST(add_close_to_max);
    ADD_TEST(add_secs_overflow);
    ADD_TEST(add_secs_and_nsecs_overflow);

    ADD_TEST(to_millis_zero);
    ADD_TEST(to_millis_secs);
    ADD_TEST(to_millis_nsecs);
    ADD_TEST(to_millis_both);
    ADD_TEST(to_millis_nsecs_rounds_up);
    ADD_TEST(to_millis_secs_overflow);
    ADD_TEST(to_millis_sum_overflows);
}
