/*
 * This file is part of flml.
 *
 * Copyright (C) 2021-2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <flml/signal.h>

#include "test.h"

void set_up(void) {
    /* empty */
}

void tear_down(void) {
    /* empty */
}

static void dummy_cb(UNUSED struct flml *flml,
                     UNUSED int signum,
                     UNUSED void *ptr) {
    //
}

START_TEST(catch_signal_0_aborts) {
    flml_signal_catch(0, dummy_cb, NULL);
}
END_TEST

START_TEST(ignore_signal_32_aborts) {
    flml_signal_ignore(32);
}
END_TEST

void add_tests(void) {
    ADD_ABORT_TEST(catch_signal_0_aborts);
    ADD_ABORT_TEST(ignore_signal_32_aborts);
}
