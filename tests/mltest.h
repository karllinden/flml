/*
 * This file is part of flml.
 *
 * Copyright (C) 2018, 2021-2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MLTEST_H
#define MLTEST_H

#include <unistd.h>

#include <flml/flml.h>

#define mltest_assert(expr) _mltest_assert(expr, #expr)

enum status {
    SUCCESS = 0,
    SET_UP_FAILED,
    SIGACTION_FAILED,
    FORK_FAILED,
    KILL_FAILED,
    FLML_FAILED,
    WAITPID_FAILED,
    TERMINATED_BY_SIGNAL,
    ASSERT_FAILED,
};

void _mltest_assert(int condition, const char *msg);

/**
 * Set up before forking and running the test.
 *
 * This function is defined by the test.
 *
 * @return zero on success, or non-zero on failure
 */
int set_up(void);

/**
 * Perform test specific actions in the target process before the main
 * loop is started.
 *
 * This function is defined by the test.
 *
 * @param flml a pointer to the flml structure
 * @return zero on success, or non-zero on failure
 */
int before_main_loop(struct flml *flml);

/**
 * Perform test specific actions in the target process after the main
 * loop has quit.
 *
 * This function is defined by the test.
 *
 * @param flml a pointer to the flml structure
 * @return the exit status of the target process
 */
int after_main_loop(struct flml *flml);

/**
 * Interact with the target process.
 *
 * This function is defined by the test.
 *
 * @param target the PID of the target process
 */
void interact(pid_t target);

#endif /* !MLTEST_H */
