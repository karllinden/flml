/*
 * This file is part of flml.
 *
 * Copyright (C) 2021-2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "rng_dima.h"
#include "rng.h"

static int is_success(const struct rng_dima *dima) {
    return dima->failure_rate <= rng_max(99);
}

static void *invoke_rng_dima(struct dima_proxy *proxy,
                             const struct dima_invocation *invocation) {
    struct rng_dima *dima = (struct rng_dima *)proxy;
    if (invocation->function == DIMA_FREE || is_success(dima)) {
        return dima_invoke(dima->next, invocation);
    } else {
        return NULL;
    }
}

void rng_dima_init(struct rng_dima *dima, struct dima *next, int failure_rate) {
    dima_init_proxy(&dima->proxy, invoke_rng_dima, DIMA_IS_THREAD_HOSTILE);
    dima->next = next;
    dima->failure_rate = failure_rate;
}
