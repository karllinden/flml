/*
 * This file is part of flml.
 *
 * Copyright (C) 2021-2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "swap_dima.h"

static void *invoke_test_dima(struct dima_proxy *proxy,
                              const struct dima_invocation *invocation) {
    struct swap_dima *dima = (struct swap_dima *)proxy;
    return dima_invoke(dima->next, invocation);
}

void swap_dima_init(struct swap_dima *dima, struct dima *next) {
    dima_init_proxy(&dima->proxy, invoke_test_dima, dima_forward_flags(next));
    dima->next = next;
}

void run_with_swapped_dima(struct swap_dima *dima,
                           struct dima *next,
                           void (*fun)(void *),
                           void *ptr) {
    struct dima *prev = dima->next;
    dima->next = next;
    fun(ptr);
    dima->next = prev;
}
