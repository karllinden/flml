/*
 * This file is part of flml.
 *
 * Copyright (C) 2021-2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef RNG_DIMA_H
#define RNG_DIMA_H

#include <dima/proxy.h>

struct rng_dima {
    struct dima_proxy proxy;
    struct dima *next;
    int failure_rate;
};

/**
 * Initialize the given DIMA to fail randomly with the given failure rate (in
 * percent).
 *
 * Successful invocations are passed onto the given §next DIMA.
 */
void rng_dima_init(struct rng_dima *dima, struct dima *next, int failure_rate);

static inline struct dima *rng_dima_to_dima(struct rng_dima *dima) {
    return dima_from_proxy(&dima->proxy);
}

#endif /* !RNG_DIMA_H */
