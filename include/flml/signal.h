/*
 * This file is part of flml.
 *
 * Copyright (C) 2018-2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _FLML_SIGNAL_H_
#define _FLML_SIGNAL_H_

#include <flml/flml.h>

/**
 * Signal callback from the main loop.
 *
 * This callback is triggered whenever a signal that has been registered with
 * flml_signal_catch has been emitted to the process. flml calls the callback
 * synchronously, after having received the signal in an asynchronous signal
 * handler and processed it internally.
 *
 * @param flml a pointer to the flml structure
 * @param signum the signal number
 * @param ptr the pointer given to flml_signal_catch
 */
typedef void flml_signal_cb(struct flml *flml, int signum, void *ptr);

/**
 * Handle a signal with its default disposition.
 *
 * A call to this function overrides any previous call to flml_signal_default,
 * flml_signal_catch or flml_signal_ignore with the same signum.
 *
 * @param signum the number of the signal to give default disposition
 */
void flml_signal_default(int signum);

/**
 * Catch a signal in the main loop and invoke a callback.
 *
 * The callback will be called synchronously when the signal has been caught and
 * processed internally by the main loop.
 *
 * A call to this function overrides any previous call to flml_signal_default,
 * flml_signal_catch or flml_signal_ignore with the same signum.
 *
 * @param signum the number of the signal to handle
 * @param cb the signal callback to attach, not NULL
 * @param ptr the pointer to pass to the callback
 */
void flml_signal_catch(int signum, flml_signal_cb *cb, const void *ptr);

/**
 * Ignore a signal.
 *
 * A call to this function overrides any previous call to flml_signal_default,
 * flml_signal_catch or flml_signal_ignore with the same signum.
 *
 * @param signum the number of the signal to ignore
 */
void flml_signal_ignore(int signum);

#endif /* !_FLML_SIGNAL_H_ */
