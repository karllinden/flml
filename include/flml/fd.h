/*
 * This file is part of flml.
 *
 * Copyright (C) 2018-2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _FLML_FD_H_
#define _FLML_FD_H_

#include <limits.h>

#include <flml/error.h>
#include <flml/flml.h>

/**
 * Mode for reading.
 *
 * When this mode is enabled the event callback will be invoked with the
 * FLML_FD_EVENT_READ event whenever there is data to read from the file
 * descriptor.
 */
#define FLML_FD_MODE_READ 0x1

/**
 * Mode for writing.
 *
 * When this mode is enabled the event callback will be invoked with the
 * FLML_FD_EVENT_WRITE event whenever data can be written to the file
 * descriptor.
 */
#define FLML_FD_MODE_WRITE 0x2

/**
 * An flml_fd_key that represents an error.
 *
 * When an object of type flml_fd_key has this value, flml_fd_key_is_error will
 * return non-zero.
 *
 * This value can be used outside flml for example to denote the absence of a
 * key, but do not compare against this value when examining the return value of
 * flml_fd_register. Use flml_fd_key_is_error instead.
 */
#define FLML_FD_KEY_ERROR UINT_MAX

/**
 * The type of the key that each system file descriptor gets when it is wrapped
 * by flml.
 *
 * Do not assume anything about this typedef. Use FLML_FD_KEY_ERROR,
 * flml_fd_key_is_error, and the other flml_fd_* functions for managing objects
 * of this type.
 */
typedef unsigned flml_fd_key;

/**
 * Enumeration of the possible events that can occur on a file descriptor.
 */
enum flml_fd_event {
    /**
     * Event that occurs when the file descriptor to poll is not open.
     */
    FLML_FD_EVENT_CLOSED,

    /**
     * Event that occurs upon an error condition on the file descriptor.
     */
    FLML_FD_EVENT_ERROR,

    /**
     * Event that occurs when a hangup occurs on the file descriptor.
     */
    FLML_FD_EVENT_HANGUP,

    /**
     * Event that occurs when there is data to read from the file descriptor.
     */
    FLML_FD_EVENT_READ,

    /**
     * Event that occurs when it is possible to write to the file descriptor.
     */
    FLML_FD_EVENT_WRITE,
};

/**
 * File descriptor event callback from the flml main loop.
 *
 * flml adds a lightweight wrapper around system file descriptors that allows an
 * event callback to be issued when the state of the underlying file descriptor
 * changes.
 *
 * It is allowed to unregister the file descriptor in any callback.
 *
 * @param flml a pointer to the flml structure in which the callback
 *             was triggered
 * @param key the key of the flml file descriptor
 * @param event the event that occured on the file descriptor
 */
typedef void flml_fd_event_cb(struct flml *flml,
                              flml_fd_key key,
                              enum flml_fd_event event);

/**
 * Register a file descriptor in the main loop.
 *
 * After registering successfully, the file descriptor is polled in the next
 * iteration of the main loop.
 *
 * This function creates a wrapper around the file descriptor inside flml and
 * returns the key that can be used to access it.
 *
 * On failure the last error is filled in. See flml_get_last_error.
 *
 * @param flml a pointer to the flml structure
 * @param fd the file descriptor to wrap
 * @param event_cb the callback to issue when an event occurs on the
 *                 underlying file descriptor
 * @return the key to the registered file descriptor on success, or an
 *         flml_fd_key representing failure; see flml_fd_key_is_error
 */
flml_fd_key flml_fd_register(struct flml *flml,
                             int fd,
                             flml_fd_event_cb *event_cb);

/**
 * Unregister a file descriptor from the main loop.
 *
 * The underlying system file descriptor is not closed.
 *
 * @param flml a pointer to the flml structure
 * @param key the key of the file descriptor to unregister, as returned by
 *            a successful call to flml_fd_register
 */
void flml_fd_unregister(struct flml *flml, flml_fd_key key);

/**
 * Get the file descriptor that was registered with the given key.
 *
 * @param flml a pointer to the flml structure
 * @param key the key of the file descriptor, as returned by a successful call
 *            to flml_fd_register
 * @return the file descriptor
 */
int flml_fd_get_fd(const struct flml *flml, flml_fd_key key);

/**
 * Set the file descriptor's user supplied pointer.
 *
 * The pointer can be accessed with flml_fd_get_ptr.
 *
 * @param flml a pointer to the flml structure
 * @param key the key of the file descriptor, as returned by a successful call
 *            to flml_fd_register
 * @param ptr the user pointer to set
 */
void flml_fd_set_ptr(struct flml *flml, flml_fd_key key, const void *ptr);

/**
 * Get the pointer that was supplied to flml_fd_set_ptr.
 *
 * @param flml a pointer to the flml structure
 * @param key the key of the file descriptor, as returned by a successful call
 *            to flml_fd_register
 * @return the pointer
 */
void *flml_fd_get_ptr(const struct flml *flml, flml_fd_key key);

/**
 * Set the mode of the flml file descriptor.
 *
 * The mode is a bitmask describing which events to poll for. If the bitmask
 * contains FLML_FD_MODE_READ the event callback will be invoked with
 * FLML_FD_EVENT_READ whenever there is data to read from the file descriptor.
 * If the bitmask contains FLML_FD_MODE_WRITE the event callback wil be invoked
 * with FLML_FD_EVENT_WRITE whenever data can be written to the file descriptor.
 *
 * @param flml a pointer to the flml structure
 * @param key the key of the file descriptor, as returned by a successful call
 *            to flml_fd_register
 * @param mode a bitmask of the mode to set
 */
void flml_fd_set_mode(struct flml *flml, flml_fd_key key, unsigned mode);

/**
 * Put the given file descriptor in non-blocking mode.
 *
 * @param fd the file descriptor to put in non-blocking mode
 * @param error a pointer to the error structure
 * @return zero on success, or non-zero otherwise, in which case the
 *         error structure is filled in appropriately
 */
int flml_fd_nonblock(int fd, struct flml_error *error);

/**
 * Determine whether the given flml file descriptor key represents an error.
 *
 * @param key the key of the file descriptor, as returned by flml_fd_register
 * @return non-zero if the flml file descriptor key represents an error, or zero
 *         otherwise
 */
static inline int flml_fd_key_is_error(flml_fd_key key) {
    return key == FLML_FD_KEY_ERROR;
}

#endif /* !_FLML_FD_H_ */
