/*
 * This file is part of flml.
 *
 * Copyright (C) 2018-2019, 2021-2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef FLML_H
#define FLML_H

#include "error.h"
#include "fd.h"
#include "sig.h"
#include "timer.h"

struct flml {
    struct fds fds;
    struct timers timers;
    struct flml_error last_error;
    struct dima *dima;
    void *ptr;
    int run;
};

void COLD NORETURN flml_fatal_impl(const char *file,
                                   int line,
                                   const char *fmt,
                                   ...);
#define flml_fatal(...) flml_fatal_impl(__FILE__, __LINE__, __VA_ARGS__)

static inline void *flml_alloc_array(struct flml *flml,
                                     size_t nmemb,
                                     size_t size) {
    void *ret = dima_alloc_array(flml->dima, nmemb, size);
    if (!ret) {
        flml_error_mem(&flml->last_error);
    }
    return ret;
}

static inline void flml_free(struct flml *flml, void *ptr) {
    dima_free(flml->dima, ptr);
}

static inline void *flml_realloc_array(struct flml *flml,
                                       void *ptr,
                                       size_t nmemb,
                                       size_t size) {
    void *ret = dima_realloc_array(flml->dima, ptr, nmemb, size);
    if (!ret) {
        flml_error_mem(&flml->last_error);
    }
    return ret;
}

#endif /* !FLML_H */
