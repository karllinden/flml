/*
 * This file is part of flml.
 *
 * Copyright (C) 2018-2019, 2021-2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <errno.h>
#include <stdio.h>
#include <string.h>

#include "error.h"
#include "flml.h"

void flml_error_mem(struct flml_error *error) {
    error->type = FLML_ERROR_MEM;
}

void flml_error_overflow(struct flml_error *error) {
    error->type = FLML_ERROR_OVERFLOW;
}

void flml_error_signal(struct flml_error *error) {
    error->type = FLML_ERROR_SIGNAL;
}

void flml_error_system(struct flml_error *error, const char *call) {
    error->type = FLML_ERROR_SYSTEM;
    error->system.call = call;
    error->system.err = errno;
}

int FLML_PUBLIC flml_error_snprint(char *str,
                                   size_t size,
                                   const struct flml_error *error) {
    switch (error->type) {
        case FLML_ERROR_NONE:
            return snprintf(str, size, "Success");
        case FLML_ERROR_MEM:
            return snprintf(str, size, "Could not allocate memory");
        case FLML_ERROR_OVERFLOW:
            return snprintf(str, size, "Computation would overflow");
        case FLML_ERROR_SIGNAL:
            return snprintf(str, size, "Could not set up signal handling");
        case FLML_ERROR_SYSTEM: {
            const struct flml_error_system *s = &error->system;
            return snprintf(
                    str, size, "%s failed: %s", s->call, strerror(s->err));
        }
    }

    flml_fatal("Invalid enum flml_error_type: %d", error->type);
}

int FLML_PUBLIC flml_error_fprint(FILE *file, const struct flml_error *error) {
#define BUFSIZE 128
    char buf[BUFSIZE];
    int result = flml_error_snprint(buf, BUFSIZE, error);
    if (result < 0) {
        return result;
    } else if (result > BUFSIZE - 1) {
        result = BUFSIZE - 1;
    }

    int r = fputs(buf, file);
    return r != EOF ? result : -result;
#undef BUFSIZE
}
