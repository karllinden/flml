/*
 * This file is part of flml.
 *
 * Copyright (C) 2018-2019, 2021-2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "flml.h"
#include "timer.h"
#include "timespec.h"

#define get_timers(flml) (&flml->timers)

static void unregistered_timer_cb(struct flml *flml,
                                  UNUSED flml_timer_key key) {
    struct timers *timers = get_timers(flml);
    assert(timers->unregistered_count > 0);
    timers->unregistered_count--;
}

static flml_timer_key timer_to_key(const struct timers *timers,
                                   const struct timer *timer) {
    return (flml_timer_key)(timer - timers->array);
}

static struct timer *key_to_timer(const struct timers *timers,
                                  flml_timer_key key) {
    return timers->array + key;
}

static struct timer *resolve_timer_key_or_null(struct flml *flml,
                                               flml_timer_key key) {
    struct timers *timers = get_timers(flml);
    if (key == 0 || key >= timers->capacity) {
        return NULL;
    }

    struct timer *result = key_to_timer(timers, key);
    return result->cb != unregistered_timer_cb ? result : NULL;
}

static struct timer *resolve_timer_key(struct flml *flml, flml_timer_key key) {
    struct timer *result = resolve_timer_key_or_null(flml, key);
    if (result == NULL) {
        flml_fatal("invalid flml_timer_key: %u", key);
    }
    return result;
}

/**
 * Returns a value less than, equal to, or greater than zero if t1 has,
 * respectively, lower priority than, equal priority to, or higher
 * priority than t2.
 */
static int compare_timers(const struct timer *t1, const struct timer *t2) {
    return -flml_timespec_compare(&t1->expiry, &t2->expiry);
}

static int compare_timer_keys(const struct timers *timers,
                              flml_timer_key key1,
                              flml_timer_key key2) {
    const struct timer *t1 = key_to_timer(timers, key1);
    const struct timer *t2 = key_to_timer(timers, key2);
    return compare_timers(t1, t2);
}

static unsigned find_highest_priority_child(const struct timers *timers,
                                            unsigned index) {
    unsigned left_index = index * 2;
    unsigned right_index = left_index + 1;

    if (left_index >= timers->size) {
        return 0;
    } else if (right_index >= timers->size) {
        return left_index;
    }

    flml_timer_key left_key = timers->heap[left_index];
    flml_timer_key right_key = timers->heap[right_index];
    if (compare_timer_keys(timers, left_key, right_key) > 0) {
        return left_index;
    } else {
        return right_index;
    }
}

static int swap_with_parent(struct timers *timers, unsigned index) {
    unsigned pindex = index / 2;
    if (pindex == 0) {
        return 0;
    }

    flml_timer_key *heap = timers->heap;
    flml_timer_key key = heap[index];
    flml_timer_key parent_key = heap[pindex];
    int swapped = compare_timer_keys(timers, key, parent_key) > 0;
    if (swapped) {
        heap[index] = parent_key;
        heap[pindex] = key;
    }
    return swapped;
}

static void swap_upwards(struct timers *timers, unsigned index) {
    while (swap_with_parent(timers, index)) {
        index /= 2;
    }
}

static void swap_downwards(struct timers *timers, unsigned index) {
    do {
        index = find_highest_priority_child(timers, index);
    } while (swap_with_parent(timers, index));
}

static void insert_key_into_heap(struct timers *timers, flml_timer_key key) {
    unsigned index = timers->size;
    timers->heap[index] = key;
    timers->size++;
    swap_upwards(timers, index);
}

static void insert_timer_into_heap(struct timers *timers, struct timer *timer) {
    flml_timer_key key = timer_to_key(timers, timer);
    insert_key_into_heap(timers, key);
}

static void remove_timer_by_index(struct timers *timers, unsigned index) {
    timers->size--;
    timers->heap[index] = timers->heap[timers->size];
    swap_downwards(timers, index);
}

static struct timer *index_to_timer(struct timers *timers, unsigned index) {
    assert(index > 0);
    assert(index < timers->size);
    return key_to_timer(timers, timers->heap[index]);
}

static struct timer *peek_timer(struct timers *timers) {
    return timers->size > 1 ? index_to_timer(timers, 1) : NULL;
}

static struct timer *pull_timer(struct timers *timers) {
    struct timer *timer = index_to_timer(timers, 1);
    remove_timer_by_index(timers, 1);
    return timer;
}

static int has_capacity(const struct flml *flml) {
    const struct timers *timers = get_timers(flml);
    if (timers->first_free != NULL) {
        assert(timers->size < timers->capacity);
        return 1;
    } else {
        assert(timers->size == timers->capacity);
        return 0;
    }
}

static int has_unregistered_timers(const struct flml *flml) {
    const struct timers *timers = get_timers(flml);
    return timers->unregistered_count > 0;
}

static void unregister_timer(struct timers *timers, struct timer *timer) {
    timer->cb = unregistered_timer_cb;
    timer->next_free = timers->first_free;
    timers->first_free = timer;
}

static void unregister_timers(struct flml *flml) {
    struct timers *timers = get_timers(flml);
    for (unsigned i = timers->size - 1; i > 0; i--) {
        struct timer *timer = index_to_timer(timers, i);
        if (timer->cb == unregistered_timer_cb) {
            remove_timer_by_index(timers, i);
            unregister_timer(timers, timer);
        }
    }
    timers->unregistered_count = 0;
}

static int increase_heap_capacity(struct flml *flml, unsigned new_capacity) {
    struct timers *timers = get_timers(flml);

    flml_timer_key *new_heap = flml_realloc_array(
            flml, timers->heap, new_capacity, sizeof(*new_heap));
    if (new_heap == NULL) {
        return 1;
    }

    timers->heap = new_heap;
    return 0;
}

static void init_free_timer(struct timer *timer) {
    timer->cb = unregistered_timer_cb;
    timer->next_free = timer + 1;
}

static void init_free_timers(struct timers *timers, unsigned new_capacity) {
    assert(timers->first_free == NULL);
    assert(timers->size == timers->capacity);
    assert(timers->capacity < new_capacity);

    struct timer *array = timers->array;
    struct timer *first_timer = array + timers->capacity;
    struct timer *last_timer = array + new_capacity - 1;
    for (struct timer *t = first_timer; t <= last_timer; t++) {
        init_free_timer(t);
    }
    timers->first_free = first_timer;
    last_timer->next_free = NULL;
}

static int increase_array_capacity(struct flml *flml, unsigned new_capacity) {
    struct timers *timers = get_timers(flml);

    struct timer *new_array = flml_realloc_array(
            flml, timers->array, new_capacity, sizeof(*new_array));
    if (new_array == NULL) {
        return 1;
    }

    timers->array = new_array;
    init_free_timers(timers, new_capacity);
    return 0;
}

static int increase_capacity(struct flml *flml) {
    struct timers *timers = get_timers(flml);

    unsigned new_capacity = timers->capacity * 2;
    if (new_capacity == 0) {
        flml_error_overflow(&flml->last_error);
        return 1;
    }

    int ret = increase_heap_capacity(flml, new_capacity)
              || increase_array_capacity(flml, new_capacity);
    if (ret) {
        return 1;
    }

    timers->capacity = new_capacity;
    return 0;
}

static int ensure_capacity(struct flml *flml) {
    if (has_capacity(flml)) {
        return 0;
    } else if (has_unregistered_timers(flml)) {
        unregister_timers(flml);
        return 0;
    } else {
        return increase_capacity(flml);
    }
}

static void init_timer(struct timer *timer,
                       const struct timespec *expiry,
                       flml_timer_cb *cb) {
    if (cb == NULL) {
        flml_fatal("flml_timer_cb may not be NULL");
    }

    flml_timespec_copy(&timer->expiry, expiry);
    timer->interval.tv_sec = -1;
    timer->interval.tv_nsec = -1;
    timer->cb = cb;
    timer->ptr = NULL;
}

static void reregister_timer(struct timers *timers, struct timer *timer) {
    int overflows
            = flml_timespec_add_non_negative(&timer->expiry, &timer->interval);
    if (overflows) {
        /*
         * This happens if the struct timespec overflows, which it will
         * only do after a very long time. Aborting here is sensible
         * since this will most likely not happen anyway.
         */
        flml_fatal("struct timespec would overflow");
    }

    insert_timer_into_heap(timers, timer);
}

static void invoke_timer(struct flml *flml, struct timer *timer) {
    struct timers *timers = get_timers(flml);
    flml_timer_key key = timer_to_key(timers, timer);
    timer->cb(flml, key);
}

static void schedule_top_timer(struct flml *flml) {
    struct timers *timers = get_timers(flml);
    struct timer *timer = pull_timer(timers);
    if (flml_timespec_is_non_negative(&timer->interval)) {
        reregister_timer(timers, timer);
    } else {
        unregister_timer(timers, timer);
    }
}

void flml_timers_init(struct timers *timers) {
    /* The first index is unused. */
    timers->size = 1;
    timers->capacity = 1;
    timers->unregistered_count = 0;
    timers->heap = NULL;
    timers->array = NULL;
    timers->first_free = NULL;
}

void flml_timers_deinit(struct flml *flml, struct timers *timers) {
    flml_free(flml, timers->heap);
    flml_free(flml, timers->array);
}

void flml_timers_trigger(struct flml *flml, struct timespec *next) {
    struct timers *timers = get_timers(flml);
    struct timer *timer;

    while ((timer = peek_timer(timers)) != NULL) {
        struct timespec now;
        flml_timespec_now(&now);

        if (flml_timespec_compare(&timer->expiry, &now) <= 0) {
            invoke_timer(flml, timer);
            schedule_top_timer(flml);
        } else {
            flml_timespec_difference(next, &timer->expiry, &now);
            return;
        }
    }
}

static struct timer *pop_first_free(struct timers *timers) {
    assert(timers->first_free != NULL);

    struct timer *timer = timers->first_free;
    timers->first_free = timer->next_free;
    return timer;
}

static flml_timer_key create_timer(struct timers *timers,
                                   const struct timespec *abs_expiry,
                                   flml_timer_cb *cb) {
    struct timer *timer = pop_first_free(timers);
    init_timer(timer, abs_expiry, cb);
    return timer_to_key(timers, timer);
}

static flml_timer_key register_timer(struct flml *flml,
                                     const struct timespec *abs_expiry,
                                     flml_timer_cb *cb) {
    struct timers *timers = get_timers(flml);
    flml_timer_key key = create_timer(timers, abs_expiry, cb);
    insert_key_into_heap(timers, key);
    return key;
}

static void require_non_negative_timespec(const struct timespec *ts) {
    if (!flml_timespec_is_non_negative(ts)) {
        flml_fatal("invalid timespec {%ld, %ld}", ts->tv_sec, ts->tv_nsec);
    }
}

FLML_PUBLIC flml_timer_key flml_timer_register(struct flml *flml,
                                               const struct timespec *expiry,
                                               flml_timer_cb *cb) {
    struct timespec abs;
    flml_timespec_now(&abs);

    require_non_negative_timespec(expiry);

    int ret = flml_timespec_add_non_negative(&abs, expiry);
    if (ret) {
        flml_error_overflow(&flml->last_error);
        return FLML_TIMER_KEY_ERROR;
    }

    ret = ensure_capacity(flml);
    if (ret) {
        return FLML_TIMER_KEY_ERROR;
    }

    return register_timer(flml, &abs, cb);
}

FLML_PUBLIC void flml_timer_unregister(struct flml *flml, flml_timer_key key) {
    struct timer *timer = resolve_timer_key(flml, key);
    timer->interval.tv_sec = -1;
    timer->interval.tv_nsec = -1;
    timer->cb = unregistered_timer_cb;

    struct timers *timers = get_timers(flml);
    timers->unregistered_count++;

    /* Strict inequality here because size includes the unused timer with index
     * 0, and unregistered_count does not. */
    assert(timers->unregistered_count < timers->size);
}

FLML_PUBLIC void flml_timer_set_ptr(struct flml *flml,
                                    flml_timer_key key,
                                    const void *ptr) {
    struct timer *timer = resolve_timer_key(flml, key);
    timer->ptr = (void *)ptr;
}

FLML_PUBLIC void *flml_timer_get_ptr(struct flml *flml, flml_timer_key key) {
    struct timer *timer = resolve_timer_key(flml, key);
    return timer->ptr;
}

FLML_PUBLIC void flml_timer_set_interval(struct flml *flml,
                                         flml_timer_key key,
                                         const struct timespec *interval) {
    require_non_negative_timespec(interval);

    struct timer *timer = resolve_timer_key(flml, key);
    flml_timespec_copy(&timer->interval, interval);
}
