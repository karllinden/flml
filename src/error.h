/*
 * This file is part of flml.
 *
 * Copyright (C) 2018-2019, 2021-2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ERROR_H
#define ERROR_H

#include <flml/error.h>

void COLD flml_error_mem(struct flml_error *error);

void COLD flml_error_overflow(struct flml_error *error);

void COLD flml_error_signal(struct flml_error *error);

void COLD flml_error_system(struct flml_error *error, const char *call);

#endif /* !ERROR_H */
