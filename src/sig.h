/*
 * This file is part of flml.
 *
 * Copyright (C) 2018-2019, 2021-2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SIG_H
#define SIG_H

#include <flml/flml.h>

/**
 * Set up a self-pipe to deliver signals to the main loop that is to be started
 * by the given flml instance.
 *
 * @param flml a pointer to the flml structure
 * @return zero on success, or non-zero otherwise in which case the last error
 *         is filled in appropriately
 */
int flml_set_up_self_pipe(struct flml *flml);

/**
 * Tear down the self-pipe that was previously set-up in the given flml
 * instance.
 *
 * @param flml a pointer to the flml structure
 */
void flml_tear_down_self_pipe(struct flml *flml);

#endif /* !SIG_H */
