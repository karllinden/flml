/*
 * This file is part of flml.
 *
 * Copyright (C) 2018-2019, 2021-2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef TIMER_H
#define TIMER_H

#include <flml/timer.h>

struct timer {
    struct timespec expiry;
    struct timespec interval;
    flml_timer_cb *cb;
    union {
        void *ptr;
        struct timer *next_free;
    };
};

/**
 * Structure that holds registered timers.
 */
struct timers {
    unsigned size;
    unsigned capacity;

    /* The number of timers that are unregistered but still in the priority
     * queue. */
    unsigned unregistered_count;

    flml_timer_key *heap;
    struct timer *array;
    struct timer *first_free;
};

void flml_timers_init(struct timers *timers);
void flml_timers_deinit(struct flml *flml, struct timers *timers);

/**
 * Trigger the timers that have expired.
 *
 * This function returns the time (relative to now) when the next timer should
 * be triggered in the struct timespec pointed to by next. If there is no next
 * timer, the struct timespec is not modified.
 */
void flml_timers_trigger(struct flml *flml, struct timespec *next);

#endif /* !TIMER_H */
