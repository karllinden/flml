/*
 * This file is part of flml.
 *
 * Copyright (C) 2018-2019, 2021-2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef TIMESPEC_H
#define TIMESPEC_H

#include <time.h>

/**
 * Get the current time.
 *
 * @param now a pointer to the struct  timespec in which to store the
 *            current time.
 */
static inline void flml_timespec_now(struct timespec *now) {
    clock_gettime(CLOCK_MONOTONIC, now);
}

/**
 * Copies the src timespec to the dest timespec.
 *
 * @param dest the destination
 * @param src the source
 */
void flml_timespec_copy(struct timespec *dest, const struct timespec *src);

/**
 * Determines whether the given struct timespec is non-negative and valid.
 *
 * A non-negative valid timespec has tv_sec >= 0, tv_nsec >= 0 and
 * tv_nsec < 1000000000.
 *
 * @param ts the timespec to check
 * @return non-zero if the struct timespec is non-negative and valid, or zero
 *         otherwise
 */
int flml_timespec_is_non_negative(const struct timespec *ts);

/**
 * Adds the non-negative addend to the augend.
 *
 * Both timespecs must be valid and the addend must be non-negative. The augend
 * and addend may not overlap.
 *
 * If the sum would overflow, the augend is left in an unspecified state.
 *
 * @param augend the timespec to add to
 * @param addend the timespec to add
 * @return zero on success, or non-zero if the sum would overflow
 */
int flml_timespec_add_non_negative(struct timespec *restrict augend,
                                   const struct timespec *addend);

/**
 * Compute the difference left - right, under the assumption that left > right.
 *
 * @param result a pointer to the struct timespec that is filled in with the
 *               result
 * @param left the first operand
 * @param right the second operand
 */
void flml_timespec_difference(struct timespec *restrict result,
                              const struct timespec *left,
                              const struct timespec *right);

/**
 * Returns a value less than, equal to, or greater than zero if ts1 is,
 * respectively, before, the same time as, or after ts2.
 */
int flml_timespec_compare(const struct timespec *ts1,
                          const struct timespec *ts2);

/**
 * Convert the time denoted by the given struct timespec to a time in
 * milliseconds rounded upwards.
 *
 * If the computation would overflow, INT_MAX is returned. tv_sec and
 * tv_nsec must be non-negative, and tv_nsec must be less than a
 * billion.
 *
 * @param ts the struct timespec to converted
 * @return the number of milliseconds
 */
int flml_timespec_to_millis(const struct timespec *ts);

#endif /* !TIMESPEC_H */
